pragma solidity ^0.4.24;
pragma experimental ABIEncoderV2;

/*
 Fix onlyverifier is not working 
*/

contract IPFSstorage{
    address admin; 
    struct document{
        address owner;
        string hash;
        string status;
    }  
    
    mapping(address => document[]) UserDocumentList;
    mapping(address => string ) private  DocumentVerifier ; 
    mapping(address => string)    VerfierList;
    bool  public  status;

   constructor() public  {
          admin = msg.sender;                             // Set the admin to contract deployer 
    } 
    
   modifier Onlyadmin( ){
       require(admin == msg.sender ,"Invalid  access");// this messahge is used to display when require fails 
       _;
   } 
   
   
    modifier Onlyverifier( ){
     //TODO to check whether veifer is  already available in mapping  
     status=false;
       require(checkDocumentVerifier(msg.sender) ,"Not a valid verifier");
       _;
   } 
    
    function testverifier() Onlyverifier () returns (bool) {  
      return   (checkDocumentVerifier(msg.sender));
    }
    
    /*stores the address of user 
    hash value of string 
     staus of current document */
    function addDocumentUser(address _add,string _hash,string _status){
        document memory Docinfo = document(_add,_hash,_status);
        UserDocumentList[_add].push(Docinfo);
    } 
 
    function getDocumentList(address _addr ) returns (document[] DocList)
    { 
         document[] memory  docInfo = UserDocumentList[_addr];
    return docInfo;
        
    }

    function  getDocumentInfo  ( document[] _userDocList) internal {
        uint totalDocuments = _userDocList.length;
        for (uint i=0; i <= totalDocuments; i++) { 
       // TODO add info       
        }
    }
        
    function getDocumentbyHashAddress(string _hash, address _add ) returns (document){
        int256  count  = -1 ; uint _docindex;
        document[]   memory DocmentListuser =   getDocumentList(_add);
        for(uint j = 0; j < DocmentListuser.length;j++)
        {    
             document  memory _doccheck = obtainDocument(DocmentListuser,j);
            if(compareString(_hash,_doccheck.hash)){ 
                _docindex=j;
                count++;
            }
        } 
    return DocmentListuser[_docindex];
    } 
    
    
    function obtainDocument(document[] _doc,uint _index)  internal returns (document){ 
 
        return (_doc[_index]);
    }
    function compareString (string a, string b)  internal pure  returns (bool){
       return keccak256(a) == keccak256(b);
   }
 
    function addVerifier(address _verifieradd,string _verifiername) Onlyadmin public {
        DocumentVerifier[_verifieradd]=_verifiername;
    }
    
     function removeVerifier(address _verifieradd) Onlyadmin  external {
        DocumentVerifier[_verifieradd]=" ";
    } 
    
    function checkDocumentVerifier(address _isverifier) view public returns(bool){
         
        if(compareString("",VerfierList[_isverifier])){
            status = true;
        } 
        return status;
    }
    
    function updateDocumentStatus(address _user ,string _hash,string _status) public  returns (document) {  
        
      for(uint k = 0; k <  getDocumentList(_user).length ;k++)
        {     
            if(compareString(_hash, obtainDocument( getDocumentList(_user),k).hash)){  
              UserDocumentList[_user][k].status=_status;
                  return obtainDocument( getDocumentList(_user),k);

            }
        } 
    } 

}
