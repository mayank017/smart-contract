pragma solidity 0.4.25;

contract escrowService {

	address public judge;
	address public buyer;
	uint256 public judgeFee;
	address public seller;
	uint256 public buyerAmountPaid; //Amount paid by buyer 
	bool public buyerPaidFee = false;
	bool public sellerPaidFee = false;
	bool public isIssueRaised = false;
	uint256 public disputeTime; // Time intervalbetween which buyer & seller need to pay Fees
	uint16 public withdraw_deposit_dispute_time_interval = 300; //Time interval x seconds after which the seller can withdrw amout once buyer paid.
	uint256 public depositTime; //UNIX time when the amount is depodsited by buyer
	address public winner;
	uint256 finalAmount = 0;
	uint256 judgeFeeReceived = 0;

	modifier isJudge() {
		require(msg.sender == judge, "Onlu judge is authorized");
		_;
	}


	function escrowService(address _seller, address _buyer, uint256 _judgeFee, uint8 _disputeTime) payable public {
		require(_judgeFee > 0, "Invalid judge fee");
		require(msg.sender != address(0));
		require(_seller != address(0), "Invalid seller address");
		require(_buyer != address(0), "Invalid buyer address");
		require(_buyer != _seller, "Both must be different ");
		require(_seller != msg.sender, "Judge cant be seller");
		require(_buyer != msg.sender, "Judge cant be buyer");
		require(_disputeTime > 0, "Inalid time");

		judge = msg.sender;
		buyer = _buyer;
		seller = _seller;
		judgeFee = _judgeFee;
		disputeTime = _disputeTime;

	}

	function raiseDispute() payable {
		require(buyer == msg.sender, "Unathorized buyer");
		require(withdraw_deposit_dispute_time_interval + depositTime > now, "Prohibition period is already over");
		require(msg.value == judgeFee, "Invalid  Fees amount");
		isIssueRaised = true;
		buyerPaidFee = true;
		judgeFeeReceived = judgeFeeReceived + msg.value;

	}


	function sellerDepositJudgeFee() payable {
		require(seller == msg.sender, "Unathorized seller");
		require(withdraw_deposit_dispute_time_interval + depositTime > now, "Prohibition period is already over");
		require(msg.value == judgeFee, "Invalid  Fees amount");
		sellerPaidFee = true;
		judgeFeeReceived = judgeFeeReceived + msg.value;

	}

	function buyerDeposit() payable {
		require(msg.value > 0, "Invalid amount");
		require(buyer == msg.sender, "Unathorized buyer");
		depositTime = now;
		buyerAmountPaid = msg.value;
	}


	function sellerWithdrawal() payable {
		require(withdraw_deposit_dispute_time_interval + depositTime < now, "Prohibition period! can't withdraw now");
		// require(isIssueRaised != true ,"dispute raised");
		require(seller == msg.sender, "Unathorized sender");

		if (!isIssueRaised) {
			seller.transfer(buyerAmountPaid);
		} else {
			require(winner == seller, "Unathorized winner");
			seller.transfer(buyerAmountPaid);
		}

	}


	function resolveDispute(address _winner) isJudge {

		require(withdraw_deposit_dispute_time_interval + depositTime < now, "Prohibition period should be over");

		if (sellerPaidFee) {
			winner = _winner;
		} else {
			winner = buyer;
			//if seller does not pay the fee, then buys wins and receives fee + amount
			// (which fee is been mention there is only jedge fee should it need to be retured ? if yes what will judge get?)
			finalAmount = buyerAmountPaid + 0;
		}

	}


	function buyerWithdrawal() payable {
		require(withdraw_deposit_dispute_time_interval + depositTime < now, "Prohibition period must be over");
		//require(isIssueRaised != true ,"dispute raised");
		require(buyer == msg.sender, "Unathorized buyer");
		require(winner == msg.sender, "Unathorized winner");
		buyer.transfer(finalAmount);
	}


	function judgeWithdrawal() payable isJudge {
		require(withdraw_deposit_dispute_time_interval + depositTime < now, "Prohibition period must be over");
		require(isIssueRaised == true, "No dispute found");
		require(winner != address(0), "Dispute not handled propely");
		judge.transfer(judgeFeeReceived);
	}

}